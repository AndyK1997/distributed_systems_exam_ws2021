# Distributed Systems ws2020/2021 Alternative Exam
This program synchronizes the gRPC access to a database with the Lamport Clock procedure.

## Getting Started :rocket:

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You need 4Gb diskspace , 8GB RAM and a CPU with 2 logic cores or better

#### Windows:
1. Installing Ubuntu Subsystem 

```
https://docs.microsoft.com/de-de/windows/wsl/install-win10
```

2. Installing Docker Desktop 

```
https://docs.docker.com/docker-for-windows/install/
```

3. Enable Scripts on Powershell Run Powershell as Admin and execute

```ps1 
Set Set-ExecutionPolicy unrestricted
```

4. Installing git

```
https://git-scm.com/book/de/v2/Erste-Schritte-Git-installieren
```

#### Linux/Debian:
1. Update Packages

```bash
$ sudo apt update && apt upgrade -y
```

2. Installing Docker and docker-compose 

```bash
$ sudo apt install docker && apt install docker-compose -y        
```

### Installing

A step by step series of examples that tell you how to get a development env running

1. Clone the repo

```bash
$ git clone https://code.fbi.h-da.de/distributed-systems/2020_wise_exam/andreas.kirchner.git
```

2. move in Directory

```bash
$ cd andreas.kirchner/
```

### Running and Stop after install

* Run
```bash
$ docker-compose up
```

* Stop
```bash
$ ctrl + c
or
$ docker-compose stop
```

### Add a Datasource

```yml
datasource_<new Id>:
   build:
     context: .
     dockerfile: ./datasources/Dockerfile
   networks: 
     vs_pruefung:
   environment: 
   - 'RPC_IP=172.0.50.1'
   - 'RPC_PORT=8080'
   - 'CSV_PATH=<your csv file with path>'
   - 'RES_ID=<new id>'
   - 'BROKER_IP=broker'
   - 'BROKER_PORT=1883'
   - 'MIN_CLIENTS=<current count clients ++>'

   depends_on: 
   - 'broker'
```
If you are add a new Datasource you must change in every Datasource the MIN_CLIENTS value to the new count.

## Built With

* [CMAKE](https://cmake.org/) - build tool
* [gRPC](https://grpc.io/docs/languages/cpp/quickstart/) - RPC framework
* [mosquitto](https://mosquitto.org/api/files/mosquitto-h.html#mosquitto_reconnect/) - MQTT brocker 
* [Docker](https://hub.docker.com/) - vm


## Author

* **Andreas Kirchner** -[staskirc](https://gitlab.fbi.h-da.de/istaskirc)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

#### gRPC
* https://grpc.io/docs/languages/cpp/quickstart/
* https://grpc.io/docs/
* https://github.com/grpc/grpc
* https://developers.google.com/protocol-buffers/docs/cpptutorial


#### MQTT
* https://mosquitto.org/api/files/mosquitto-h.html#mosquitto_lib_init
* https://mosquitto.org/blog/
* https://mosquitto.org/api/files/mosquitto-h.html#mosquitto_reconnect

#### Docker
* https://linuxhint.com/mysql_docker_compose/
