//
// Created by AndyK on 08.03.2021.
//

#include "MqttPublisher.h"


/**
 * Constructor
 * @param mosqID =id for publisher(0)
 * @param brokerIp =ip for MQTT Broker(localhost)
 * @param brokerPort
 */
MqttPublisher::MqttPublisher(std::string mosqID, std::string brokerIp, int brokerPort) {
    std::cout << "Hello from Constructor Mqtt" << mosqID << " " << brokerIp << " " << brokerPort << std::endl;
    this->brokerIp = brokerIp;
    this->brokerPort = brokerPort;
    this->sourceId = stoi(mosqID);
}

/**
 * Destructor (destroy and clean up MQTT socket )
 */
MqttPublisher::~MqttPublisher() {

    mosquitto_destroy(this->mosq);
    mosquitto_lib_cleanup();
}

/**
 * SEND MQTT Request(dataSourceId, clock) to MQTTBroker
 * @param clockEventNumber = lamport Clock
 * @return Connection state if(SUCCESS 0, FAILURE -1)
 */
int MqttPublisher::sendRequestToBroker(int clockEventNumber) {
    int id = mqttIDCounter + mqttIDCounter * sourceId;
    mosquitto_lib_init();
    this->mosq = mosquitto_new(std::to_string(id).c_str(), true, NULL);
    mqttIDCounter++;
    //PAYLOAD
    std::string msg = std::to_string(sourceId) + "|" + std::to_string(clockEventNumber);

    //TRANSMISSION STATE
    this->rc = mosquitto_connect(this->mosq, this->brokerIp.c_str(), this->brokerPort, 20);

    //CONNECTION FAILURE
    if (this->rc != 0) {
        std::cerr << "Client couldt connect to broker publisher! Error Code:" << this->rc << std::endl;
        mosquitto_destroy(this->mosq);
        return -1;
        //CONNECTION SUCCESS
    } else {
        std::cout << "connected send Data: " << msg << std::endl;
        mosquitto_publish(this->mosq, &id, "Event", msg.size(), msg.c_str(), 0, false);
        mosquitto_disconnect(this->mosq);
        mosquitto_destroy(this->mosq);
        mosquitto_lib_cleanup();
    }
    return 0;
}


/**
 * SEND MQTT Release(dataSourceId, "release")
 * @return Connection state if(SUCCESS 0, FAILURE -1)
 */
int MqttPublisher::sendReleaseToBroker() {

    int id = mqttIDCounter + mqttIDCounter * sourceId;
    mosquitto_lib_init();
    this->mosq = mosquitto_new(std::to_string(id).c_str(), true, NULL);
    mqttIDCounter++;
    //PAYLOAD
    std::string msg = std::to_string(sourceId) + '|' + "release";

    //TRANSMISSION STATE
    this->rc = mosquitto_connect(this->mosq, this->brokerIp.c_str(), this->brokerPort, 20);

    //CONNECTION FAILURE
    if (this->rc != 0) {
        std::cerr << "Client couldt connect to broker publisher release! Error Code:" << this->rc << std::endl;
        mosquitto_destroy(this->mosq);
        return -1;
        //CONNECTION SUCCESS
    } else {
        std::cout << "connected send Data: " << msg << std::endl;
        mosquitto_publish(this->mosq, &id, "Release", msg.size(), msg.c_str(), 0, false);
        mosquitto_disconnect(this->mosq);
        mosquitto_destroy(this->mosq);
        mosquitto_lib_cleanup();
    }
    return 0;
}


/**
 * SEND MQTT Ack(dataSourceId, requestedSourceId, Clock)
 * @param requestedSourceId = dataSource who will work on this clock time
 * @param clock lamportClock
 * @return Connection state if(SUCCESS 0, FAILURE -1)
 */
int MqttPublisher::sendSendACK(int requestedSourceId, int clock) {
    int id = mqttIDCounter + mqttIDCounter * sourceId;
    mosquitto_lib_init();
    this->mosq = mosquitto_new(std::to_string(id).c_str(), true, NULL);
    mqttIDCounter++;
    //PAYLOAD
    std::string msg = std::to_string(sourceId) + '|' + std::to_string(requestedSourceId) + '|' + std::to_string(clock);

    //TRANSMISSION STATE
    this->rc = mosquitto_connect(this->mosq, this->brokerIp.c_str(), this->brokerPort, 20);

    //CONNECTION FAILURE
    if (this->rc != 0) {
        std::cerr << "Client couldt connect to broker publisher release! Error Code:" << this->rc << std::endl;
        mosquitto_destroy(this->mosq);
        return -1;
        //CONNECTION SUCCESS
    } else {
        std::cout << "connected send Data: " << msg << std::endl;
        mosquitto_publish(this->mosq, &id, "ACK", msg.size(), msg.c_str(), 0, false);
        mosquitto_disconnect(this->mosq);
        mosquitto_destroy(this->mosq);
        mosquitto_lib_cleanup();
    }
    return 0;
}
