//
// Created by AndyK on 10.03.2021.
//
#include "PublishMqttMessages.h"

void sendAck(int id, int clock) {
    std::scoped_lock<std::mutex> lock(mqttMutex);
    mqttPublisher->sendSendACK(id, clock);
}

void sendEvent(int clock) {
    std::scoped_lock<std::mutex> lock(mqttMutex);
    mqttPublisher->sendRequestToBroker(clock);
}

void sendRelease() {
    std::scoped_lock<std::mutex> lock(mqttMutex);
    mqttPublisher->sendReleaseToBroker();
}

