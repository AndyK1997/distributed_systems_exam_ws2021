//
// Created by AndyK on 08.03.2021.
//

#ifndef DATASOURCE_MQTTPUBLISHER_H
#define DATASOURCE_MQTTPUBLISHER_H

#include <string>
#include <mosquitto.h>
#include <iostream>

inline static int mqttIDCounter = 1000;

class MqttPublisher {
private:
    int rc;
    struct mosquitto *mosq;
    std::string brokerIp;
    int brokerPort;
    int sourceId;
public:
    MqttPublisher(std::string mosqID, std::string brokerIp, int brokerPort);

    ~MqttPublisher();

    int sendRequestToBroker(int clockEventNumber);

    int sendReleaseToBroker();

    int sendSendACK(int requestedSourceId, int clock);

};


#endif //DATASOURCE_MQTTPUBLISHER_H
