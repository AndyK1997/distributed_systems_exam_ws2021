#include <iostream>
#include "CsvFile.h"
#include "Transmission.h"
#include "MqttPublisher.h"
#include <thread>
#include <chrono>
#include <queue>
#include <mutex>
#include "PublishMqttMessages.h"
#include <mqtt_protocol.h>
#include "LamportClock.h"

static int datasourceId;
LamportClock *lamportClock;
static std::mutex lamportMutex;


/**
 * Check if yourTurn
 * @return
 */
bool checkCv() {
    if (waitUntilRelease) {
        if (lamportClock->checkYourTurn()) {
            if (lamportClock->checkAck(lamportClock->getLamportQueue().front().clockEventNumber)) {
                return true;
            }
        }
    }
    return false;
}

/**
 * MQTT Callback handler for Broker Connection to subscribe a topic
 * @param mosq
 * @param obj
 * @param rc
 */
void on_connect(struct mosquitto *mosq, void *obj, int rc) {
    //CONNECTION FAILURE
    if (rc != 0) {
        std::cerr << "Error with result code:" << rc << std::endl;
        exit(EXIT_FAILURE);
    }

    //CONNECTION SUCCESS
    mosquitto_subscribe_v5(mosq, NULL, "Event", 0, MQTT_SUB_OPT_SEND_RETAIN_NEW, NULL);
    mosquitto_subscribe_v5(mosq, NULL, "Release", 0, MQTT_SUB_OPT_SEND_RETAIN_NEW, NULL);
    mosquitto_subscribe_v5(mosq, NULL, "ACK", 0, MQTT_SUB_OPT_SEND_RETAIN_NEW, NULL);
}

/**
 * MQTT Callback handler for incoming messages
 * @param mosq
 * @param obj
 * @param msg
 */
void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *msg) {
    std::string message = reinterpret_cast<char *>(msg->payload);
    std::string topic(msg->topic);
    int success = -200;

    if ("Event" == topic) {
        std::string id, clock;
        std::stringstream buffer(message);
        getline(buffer, id, '|');
        getline(buffer, clock, '|');
        int idInt = stoi(id), clockInt = stoi(clock);

        std::scoped_lock<std::mutex> lock(lamportMutex);
        success = lamportClock->insertInEventQueue({idInt, clockInt});

    } else if ("Release" == topic) {
        std::scoped_lock<std::mutex> lock(lamportMutex);
        success = lamportClock->release();


    } else if ("ACK" == topic) {
        std::string id, clock, regId;
        std::stringstream buffer(message);
        getline(buffer, id, '|');
        getline(buffer, regId, '|');
        getline(buffer, clock, '|');
        int idInt = stoi(id), clockInt = stoi(clock), regIdInt = stoi(regId);
        if (regIdInt == datasourceId) {
            std::scoped_lock<std::mutex> lock(lamportMutex);
            success = lamportClock->insertAckInQueue({idInt, regIdInt, clockInt});
        }
    } else {
        std::cerr << "Invalid topic received: " << msg->topic << std::endl;
    }
    cvSend.notify_one();
}


/**
 * Subscribe each predefined Topic in a infinity loop
 * @param brokerIp
 * @param brokerPort
 * @param sourceId
 */
void mqttSubscriberThread(std::string brokerIp, std::string brokerPort, std::string sourceId) {
    int rc, id = stoi(sourceId);
    mosquitto_lib_init();
    struct mosquitto *mosq;
    std::string subID = "sub" + sourceId;
    mosq = mosquitto_new(subID.c_str(), true, &id);
    mosquitto_connect_callback_set(mosq, on_connect);
    mosquitto_message_callback_set(mosq, on_message);
    rc = mosquitto_connect(mosq, brokerIp.c_str(), stoi(brokerPort), 30);

    //CONNECTION FAILURE
    if (rc != 0) {
        std::cerr << "Couldt not connect to Broker with return code: " << rc << std::endl;

        //CONNECTION SUCCESS
    } else {
        mosquitto_loop_forever(mosq, -1, 1);
        mosquitto_loop_stop(mosq, true);
        mosquitto_disconnect(mosq);
        mosquitto_destroy(mosq);
        mosquitto_lib_cleanup();
    }
    std::cout << "Exit Subscriber" << std::endl;

}

/**
 * Create new Events and Release Passed Events
 * @param csv =csv File containing  Sleeptime and FileSize
 * @param transmission = logic for gRPC
 */
[[noreturn]] void createEventsThread(CsvFile &csv, Transmission &transmission) {
    while (true) {
        int recordIndex = 0;
        bool nextRecord = true;
        while (recordIndex < csv.getCsvData().size()) {
            if (nextRecord) {
                std::scoped_lock<std::mutex> lock(lamportMutex);
                sendEvent(clockEventNumber);
                nextRecord = false;
            }


            //std::this_thread::sleep_for(std::chrono::seconds(1)); //give other Thread a chance to require a lock
            // std::unique_lock<std::mutex> cvlock(lamportMutex);
            //cvSend.wait(cvlock, [] { return checkCv(); });

            if (checkCv()) {
                int currentClockNumber = lamportClock->getLamportQueue().front().clockEventNumber;
                waitUntilRelease = false;
                std::this_thread::sleep_for(std::chrono::seconds(csv.getCsvData().at(recordIndex).sleepTime));
                //gRPC

                transmission.healthCheck();
                transmission.storeDataOnStorage(datasourceId, csv.getCsvData().at(recordIndex).fileSize);

                sendRelease();
                //
                std::cout << "PROCESSING CLOCK ID: " << currentClockNumber << std::endl;
                nextRecord = true;
                recordIndex++;

                //cvlock.unlock();
            }
        }
    }
}


/**
 * Main
 * @param argc 7
 * @param argv grpcIp(172.0.50.1), grpcPort(8080), csvFilePath(%/vs_data.csv), sourceId(0), mqttBrokerIp(localhost), brokerPort(1883), minClients(3)
 * @return
 */
int main(int argc, char *argv[]) {
    std::string grpcIP = argv[1], grpcPort = argv[2], csvFilePath = argv[3], sourceId = argv[4], brokerIp = argv[5], brokerPort = argv[6];
    std::string minClients = argv[7];
    std::string grpcAddress(grpcIP + ":" + grpcPort);
    datasourceId = stoi(sourceId);
    CsvFile csv(csvFilePath);
    csv.readCsvFile();
    mqttPublisher = new MqttPublisher(std::to_string(datasourceId), brokerIp, stoi(brokerPort));
    mqttPublisher2 = new MqttPublisher(std::to_string(datasourceId), brokerIp, stoi(brokerPort));
    Transmission transmission(grpc::CreateChannel(grpcAddress, grpc::InsecureChannelCredentials()));
    lamportClock = new LamportClock(datasourceId, stoi(minClients));


    std::cout << "DataSource ID: " << sourceId << " registered" << std::endl;
    std::thread t2(mqttSubscriberThread, ref(brokerIp), ref(brokerPort), ref(sourceId));

    std::cout << "starting please wait 15 sec..." << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(15));

    std::thread t1(createEventsThread, std::ref(csv), std::ref(transmission));

    t1.join();
    t2.join();

    return 0;
}
