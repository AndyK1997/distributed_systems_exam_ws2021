//
// Created by AndyK on 08.03.2021.
//

#include "Transmission.h"

/**
 * Store Data via gRPC on DataStorage
 * @param id = dataSourceId
 * @param size = size defined in csvFile
 */
void Transmission::storeDataOnStorage(int id, int size) {
    Resource resource;
    resource.set_id(id);
    resource.set_name("DataSource: " + std::to_string(id));
    resource.set_size(size);
    resource.set_description(
            "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut");

    Reply reply;
    Status status;
    ClientContext clientContext;
    status = stub_->storeData(&clientContext, resource, &reply);
    if (!status.ok()) {
        std::cerr << "Error in gRPC code:" << status.error_code() << std::endl;
    } else {
        // evalutate reply
        std::cout << "Server Answer: " << reply.description();
        std::cout << "Transmission Succesfull" << std::endl;
    }
}


/**
 * Get Data via gRPC from Server
 */
void Transmission::getDataFromStorage() {
    ResourceRequest resourceRequest;
    Resource resource;
    Status status;
    ClientContext clientContext;
    status = stub_->getData(&clientContext, resourceRequest, &resource);
    if (!status.ok()) {
        std::cerr << "Error in Transmission" << std::endl;
    } else {
        // evalutate resource
        std::cout << "Transmission Succesfull" << std::endl;
    }

}

/**
 * Get a Collection of Resources from DataStorage via gRPC
 */
void Transmission::listDataFromStorage() {
    ResourcesRequest resourcesRequest;
    ClientContext clientContext;
    Resource resource;
    std::unique_ptr<grpc::ClientReader<Resource>> reader(stub_->listData(&clientContext, resourcesRequest));

    while (reader->Read(&resource)) {
        std::cout << "Client: received value " << resource.name() << std::endl;
        //Single Resources evaluate here
    }

    Status status=reader->Finish();
    if(status.ok()){
        std::cout << "Client: tracing succeeded" << std::endl;
    }else{
        std::cout << "Client: tracing failed" << std::endl;
    }


}

/**
 * Check via gRPC if DataSource is Available
 */
void Transmission::healthCheck() {
    HealthCheckRequest healthCheckRequest;
    Reply reply;
    Status status;
    ClientContext clientContext;
    status=stub_->healthCheck(&clientContext,healthCheckRequest, &reply);
    if(!status.ok()){
        std::cerr << "Error in gRPC HealthCheck code:" << status.error_code() << std::endl;
    }else {
        // evalutate reply
        std::cout << "HealthCheck SUCCESSFUL" << std::endl;
        std::cout << "Status: " << reply.status() << ", Description: " << reply.description() << std::endl;
    }
}