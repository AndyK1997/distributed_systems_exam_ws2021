//
// Created by AndyK on 08.03.2021.
//

#ifndef DATASOURCE_TRANSMISSION_H
#define DATASOURCE_TRANSMISSION_H
#include <grpcpp/grpcpp.h>
#include "rpcService.pb.h"
#include "rpcService.grpc.pb.h"


using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using fixInvalidPackage::Resource;
using fixInvalidPackage::Resources;
using fixInvalidPackage::Request;
using fixInvalidPackage::ResourceRequest;
using fixInvalidPackage::ResourcesRequest; //Empty
using fixInvalidPackage::HealthCheckRequest; //Empty
using fixInvalidPackage::rpcService;
using fixInvalidPackage::Reply;

class Transmission {
private:
   std::unique_ptr<rpcService::Stub> stub_;
public:
     Transmission(std::shared_ptr<Channel> channel) : stub_(rpcService::NewStub(channel)){}
     void storeDataOnStorage(int id, int size);

    void getDataFromStorage();
     void listDataFromStorage();
     void healthCheck();
};


#endif //DATASOURCE_TRANSMISSION_H
