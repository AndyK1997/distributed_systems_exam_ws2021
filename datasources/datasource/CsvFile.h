//
// Created by AndyK on 08.03.2021.
//

#ifndef DATASOURCE_CSVFILE_H
#define DATASOURCE_CSVFILE_H
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>

struct Record{
    int sleepTime;
    int fileSize;
};

class CsvFile {
private:
    std::vector<Record> csvData;
public:
    const std::vector<Record> &getCsvData() const;

private:
    std::string csvPath;
public:
    CsvFile(std::string path) : csvPath(path) {}

    void readCsvFile();

    void printCsvFile();

};


#endif //DATASOURCE_CSVFILE_H
