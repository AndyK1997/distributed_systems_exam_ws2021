//
// Created by AndyK on 08.03.2021.
//

#include "CsvFile.h"


void CsvFile::readCsvFile() {
     std::ifstream FILE;
     FILE.open(csvPath);
     if(!FILE.fail()){
         csvData.clear();
         while(FILE.peek()!=EOF){
             std::string line;
             getline(FILE,line);
             std::stringstream buffer(line);
             std::string sleepTime, fileSize;
             getline(buffer,sleepTime,',');
             getline(buffer,fileSize,',');
             csvData.push_back({stoi(sleepTime),stoi(fileSize)});
         }
         printCsvFile();
     } else {
         std::cerr << "Cant open csv File" << std::endl;
     }
}

void CsvFile::printCsvFile() {
    for (Record r : csvData) {
        std::cout << "sleeptime: " << r.sleepTime << ", fileSize: " << r.fileSize << std::endl;
    }
}

const std::vector<Record> &CsvFile::getCsvData() const {
    return csvData;
}
