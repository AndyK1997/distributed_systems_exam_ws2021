//
// Created by AndyK on 10.03.2021.
//

#ifndef DATASOURCE_PUBLISHMQTTMESSAGES_H
#define DATASOURCE_PUBLISHMQTTMESSAGES_H

#include <mutex>
#include "MqttPublisher.h"

inline MqttPublisher *mqttPublisher;
inline MqttPublisher *mqttPublisher2;
inline std::mutex mqttMutex;

void sendAck(int id, int clock);

void sendEvent(int clock);

void sendRelease();

#endif //DATASOURCE_PUBLISHMQTTMESSAGES_H