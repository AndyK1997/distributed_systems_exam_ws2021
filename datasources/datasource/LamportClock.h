//
// Created by AndyK on 10.03.2021.
//

#ifndef DATASOURCE_LAMPORTCLOCK_H
#define DATASOURCE_LAMPORTCLOCK_H

#include <vector>
#include <mutex>
#include <iostream>
#include "PublishMqttMessages.h"
#include <algorithm>
#include <condition_variable>
#include <condition_variable>

struct Event {
    int sourceID;
    int clockEventNumber;
};

struct ACK {
    int sourceId;
    int requestSourceId;
    int clock;
};

inline std::condition_variable cvSend;
inline int clockEventNumber = 0;
inline bool waitUntilRelease = true;
inline std::mutex ackMutex;

class LamportClock {
private:
    std::vector<ACK> ackQueue;
    std::vector<Event> lamportQueue;
    std::mutex lamportQueueMutex;
    //std::mutex ackMutex;
    static int minCountConnectedClients;
    int dataSourceId;


public:
    LamportClock(int datasource, int minCountConnectedClients);

    bool checkAck(int clock);

    int insertInEventQueue(Event event);

    int release();

    int insertAckInQueue(ACK ack);

    bool checkYourTurn();

    const std::vector<Event> &getLamportQueue() const;

    const std::vector<ACK> &getAckQueue() const;
};


#endif //DATASOURCE_LAMPORTCLOCK_H
