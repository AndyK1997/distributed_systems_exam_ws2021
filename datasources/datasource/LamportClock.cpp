//
// Created by AndyK on 10.03.2021.
//

#include "LamportClock.h"

//Count of Connected DataSources
int LamportClock::minCountConnectedClients = 0;

/**
 * Constructor
 * @param datasource = dataSourceId
 * @param minCountConnectedClients = min Count of DataSources
 */
LamportClock::LamportClock(int datasource, int minCountConnectedClients) {
    this->dataSourceId = datasource;
    this->minCountConnectedClients = minCountConnectedClients;
}


/**
 * Check if all Ack of Current Event were received
 * @param clock
 * @return
 */
bool LamportClock::checkAck(int clock) {
    int countRecivedAck = 0;
    std::scoped_lock<std::mutex> lock(ackMutex);
    std::cout << "-----------------------Start ACK Queue--------------------------" << std::endl;
    for (ACK a : ackQueue) {
        std::cout << "source ID " << a.sourceId << ", RegSource " << a.requestSourceId
                  << ", clock " << a.clock << std::endl;
        if (a.requestSourceId == dataSourceId && a.clock == clock) {
            countRecivedAck++;
        }
    }
    std::cout << "-----------------------------------------------------------------" << std::endl;
    if (countRecivedAck == minCountConnectedClients) {
        int x = 0;
        while (true) {
            for (auto x = 0; x < ackQueue.size(); x++) {
                if (ackQueue.at(x).clock == clock) {
                    ackQueue.erase(ackQueue.begin() + x);
                    x++;
                }
            }
            if (x == ackQueue.size()) {
                break;
            }
        }
        std::cout << "Delete ackQueue" << std::endl;
        //ackQueue.clear();
        return true;
    }
    return false;
}


/**
 * Insert new Event in Queue
 * @param event = received event
 */
int LamportClock::insertInEventQueue(Event event) {
    lamportQueue.push_back(event);
    //sendAck(event.sourceID, event.clockEventNumber);
    mqttPublisher2->sendSendACK(event.sourceID, event.clockEventNumber);

    std::sort(lamportQueue.begin(), lamportQueue.end(), [](const Event &x, const Event &y) {
        return x.clockEventNumber < y.clockEventNumber;
    });

    clockEventNumber = lamportQueue[lamportQueue.size() - 1].clockEventNumber + 1;

    std::cout << "--------------------------Start Queue----------------------------" << std::endl;
    for (Event e : lamportQueue) {
        if (e.sourceID > minCountConnectedClients) {
            minCountConnectedClients = e.sourceID;
        }
        std::cout << "ID:" << e.sourceID << ", Clock:" << e.clockEventNumber << std::endl;
    }
    std::cout << "------------------------------------------------------------------" << std::endl;
    return 0;
}

/**
 * Send a Release to all Clients in Process is finished
 */
int LamportClock::release() {
    lamportQueue.erase(lamportQueue.begin());
    waitUntilRelease = true;
    return 0;
}

/**
 * Insert received Ack in Ack Queue
 * @param ack =Acknowledge Message
 */
int LamportClock::insertAckInQueue(ACK ack) {
    this->ackQueue.push_back(ack);
    return 0;
}

/**
 * Check if you are First in Queue
 * @return
 */
bool LamportClock::checkYourTurn() {
    bool returnValue = false;
    if (!lamportQueue.empty() && waitUntilRelease) {
        if (lamportQueue.front().sourceID == dataSourceId) {
            returnValue = true;
        }
    }
    return returnValue;
}

/**
 * get Event Queue
 * @return
 */
const std::vector<Event> &LamportClock::getLamportQueue() const {
    return lamportQueue;
}

/**
 * get Ack Queue
 * @return
 */
const std::vector<ACK> &LamportClock::getAckQueue() const {
    return ackQueue;
}

